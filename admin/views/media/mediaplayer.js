$(function () {
    // 普通媒体播放器
    $('#generalMediaPlayer').kendoMediaPlayer({
        autoPlay: false,
        navigatable: true,
        volume: 60,
        media: {
            title: '圣斗士星矢：圣域传说',
            source: 'https://vfx.mtime.cn/Video/2016/01/29/mp4/160129092749708467.mp4'
        }
    });
    // 播放列表
    $('#playList').kendoListView({
        dataSource: {
            data: [
                {
                    title: '圣斗士星矢：圣域传说',
                    name: 'Saint Seiya: Legend of Sanctuary',
                    year: '2014',
                    length: '93',
                    type: '动画',
                    director: '佐藤敬一',
                    actor: '石川界人、佐佐木彩夏',
                    poster: 'http://img31.mtime.cn/mg/2016/02/23/094554.31037035_270X405X4.jpg',
                    time: '00:29',
                    source: 'https://vfx.mtime.cn/Video/2016/01/29/mp4/160129092749708467.mp4'
                },
                {
                    title: '龙珠超：布罗利',
                    name: 'Dragon Ball Super: Broly',
                    year: '2018',
                    length: '100',
                    type: '动画',
                    director: '长峰达也',
                    actor: '野泽雅子、堀川亮',
                    poster: 'http://img5.mtime.cn/mg/2019/05/22/105854.23721210_270X405X4.jpg',
                    time: '01:29',
                    source: 'https://vfx.mtime.cn/Video/2019/05/13/mp4/190513214658178405.mp4'
                },
                {
                    title: '阿凡达',
                    name: 'Avatar',
                    year: '2009',
                    length: '162',
                    type: '科幻',
                    director: '詹姆斯·卡梅隆',
                    actor: '萨姆·沃辛顿、佐伊·索尔达娜',
                    poster: 'http://img31.mtime.cn/mg/2014/09/01/221450.72496694_270X405X4.jpg',
                    time: '00:35',
                    source: 'https://vfx.mtime.cn/Video/2009/12/08/mp4/091208175503041222.mp4'
                },
                {
                    title: '战狼2',
                    name: 'Wolf Warriors Ⅱ',
                    year: '2017',
                    length: '123',
                    type: '动作',
                    director: '吴京',
                    actor: '吴京、弗兰克·格里罗',
                    poster: 'http://img5.mtime.cn/mg/2017/07/21/162643.61740466_270X405X4.jpg',
                    time: '02:13',
                    source: 'https://vfx.mtime.cn/Video/2017/08/17/mp4/170817105313150319.mp4'
                },
                {
                    title: '哪吒之魔童降世',
                    name: 'Ne Zha',
                    year: '2019',
                    length: '110',
                    type: '动画',
                    director: '饺子',
                    actor: '吕艳婷、囧森瑟夫',
                    poster: 'http://img5.mtime.cn/mg/2019/07/31/110053.38913357_270X405X4.jpg',
                    time: '02:20',
                    source: 'https://vfx.mtime.cn/Video/2019/07/25/mp4/190725150727428271.mp4'
                },
                {
                    title: '流浪地球',
                    name: 'The Wandering Earth',
                    year: '2019',
                    length: '125',
                    type: '科幻',
                    director: '郭帆',
                    actor: '吴京、屈楚萧',
                    poster: 'http://img5.mtime.cn/mg/2019/01/30/152307.77354514_270X405X4.jpg',
                    time: '02:01',
                    source: 'https://vfx.mtime.cn/Video/2019/01/21/mp4/190121221027701384.mp4'
                },
                {
                    title: '复仇者联盟4：终局之战',
                    name: 'Avengers: Endgame',
                    year: '2019',
                    length: '181',
                    type: '奇幻',
                    director: '安东尼·罗素',
                    actor: '小罗伯特·唐尼、克里斯·埃文斯',
                    poster: 'http://img5.mtime.cn/mg/2019/03/29/095612.14234221_270X405X4.jpg',
                    time: '02:30',
                    source: 'https://vfx.mtime.cn/Video/2019/03/14/mp4/190314223540373995.mp4'
                },
                {
                    title: '红海行动',
                    name: 'Operation Red Sea',
                    year: '2018',
                    length: '138',
                    type: '战争',
                    director: '林超贤',
                    actor: '张译、黄景瑜',
                    poster: 'http://img5.mtime.cn/mg/2018/02/17/150051.13993864_270X405X4.jpg',
                    time: '01:35',
                    source: 'https://vfx.mtime.cn/Video/2017/12/12/mp4/171212104101581428.mp4'
                },
                {
                    title: '我和我的祖国',
                    name: 'My People，My Country',
                    year: '2019',
                    length: '155',
                    type: '剧情',
                    director: '陈凯歌',
                    actor: '黄渤、张译',
                    poster: 'http://img5.mtime.cn/mg/2019/09/26/092514.83698073_270X405X4.jpg',
                    time: '02:58',
                    source: 'https://vfx.mtime.cn/Video/2019/09/27/mp4/190927155106260531.mp4'
                },
                {
                    title: '中国机长',
                    name: 'The Captain',
                    year: '2019',
                    length: '111',
                    type: '灾难',
                    director: '刘伟强',
                    actor: '张涵予、欧豪',
                    poster: 'http://img5.mtime.cn/mg/2019/09/18/095840.16076538_270X405X4.jpg',
                    time: '00:59',
                    source: 'https://vfx.mtime.cn/Video/2019/08/05/mp4/190805144313776004.mp4'
                },
                {
                    title: '泰坦尼克号',
                    name: 'Titanic',
                    year: '1997',
                    length: '194',
                    type: '灾难',
                    director: '詹姆斯·卡梅隆',
                    actor: '昂纳多·迪卡普里奥、凯特·温丝莱特',
                    poster: 'http://img5.mtime.cn/mg/2017/02/17/164948.32049628_270X405X4.jpg',
                    time: '04:10',
                    source: 'https://vfx.mtime.cn/Video/2008/09/22/mp4/080922112651306128.mp4'
                },
                {
                    title: '星球大战：天行者崛起',
                    name: 'Star Wars: The Rise of Skywalker',
                    year: '2019',
                    length: '141',
                    type: '科幻',
                    director: 'J·J·艾布拉姆斯',
                    actor: '黛茜·雷德利、亚当·德赖弗',
                    poster: 'http://img5.mtime.cn/mg/2019/11/29/102829.55283301_270X405X4.jpg',
                    time: '00:59',
                    source: 'https://vfx.mtime.cn/Video/2019/11/29/mp4/191129101921978666_1080.mp4'
                }
            ]
        },
        template: kendo.template(
            '<div class="row no-gutters">' +
                '<div class="col-3 p-2">' +
                    '<img class="w-100" src="#: poster #">' +
                    '<time>#: time #</time>' +
                '</div>' +
                '<div class="col-9 p-2">' +
                    '<h4 class="font-weight-bold text-dark">#: title #</h4>' +
                    '<h5 class="font-weight-light text-black-50">#: name #</h5>' +
                    '<p class="mt-3 mb-1">上映：#: year # 年</p>' +
                    '<p class="mb-1">片长：#: length # 分钟</p>' +
                    '<p class="mb-1">类型：#: type #</p>' +
                    '<p class="mb-1">导演：#: director #</p>' +
                    '<p class="mb-0">主演：#: actor #</p>' +
                '</div>' +
            '</div>'
        ),
        scrollable: true,
        selectable: true,
        change: function () {
            $('#generalMediaPlayer').data('kendoMediaPlayer').media(this.dataSource.view()[this.select().index()]);
        },
        dataBound: function () {
            this.select(this.element.children().first());
        }
    });
});