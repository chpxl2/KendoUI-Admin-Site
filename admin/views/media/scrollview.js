$(function () {
    // DOM 滚动视图
    $('#domScrollView').kendoScrollView();
    // 普通滚动视图
    $('#generalScrollView').kendoScrollView({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                total: 'total',
                data: 'data'
            },
            pageSize: 1
        },
        contentHeight: '100%',
        template:
            '<div class="text-center">' +
                '<img src="#= data.photo.url #" alt="#= data.nickName #">' +
                '<picture style="background-image: url(\'#= data.photo.url #\');"></picture>' +
            '</div>',
        emptyTemplate:
            '<div class="text-center">' +
                '<img src="img/avatar.png" alt="Avatar">' +
                '<picture style="background-image: url(\'img/avatar.png\');"></picture>' +
            '</div>'
    });
    // 指定页滚动视图
    $('#pageScrollView').kendoScrollView({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                total: 'total',
                data: 'data'
            },
            pageSize: 1
        },
        template:
            '<div class="text-center">' +
                '<img src="#= data.photo.url #" alt="#= data.nickName #">' +
                '<picture style="background-image: url(\'#= data.photo.url #\');"></picture>' +
            '</div>',
        page: 4
    });
    // 无圆点滚动视图
    $('#noDotScrollView').kendoScrollView({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                total: 'total',
                data: 'data'
            },
            pageSize: 1
        },
        template:
            '<div class="text-center">' +
                '<img src="#= data.photo.url #" alt="#= data.nickName #">' +
                '<picture style="background-image: url(\'#= data.photo.url #\');"></picture>' +
            '</div>',
        enablePager: false
    });
    // 标题滚动视图
    $('#titleScrollView').kendoScrollView({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                total: 'total',
                data: 'data'
            },
            pageSize: 1
        },
        template:
            '<div class="d-flex justify-content-center align-items-center h-100">' +
                '<img class="img-thumbnail" src="#= data.photo.url #" alt="#= data.nickName #">' +
                '<picture style="background-image: url(\'#= data.photo.url #\');"></picture>' +
                '<div class="title">' +
                    '<h5 class="mb-1">#= data.realName #<small class="font-weight-light text-white-50 ml-2">#= data.nickName #</small></h5>' +
                    '<p class="m-0"><small>#= summary #</small></p>' +
                '</div>' +
            '</div>',
        contentHeight: '100%',
        enablePager: false
    });
    // 分组滚动视图
    $('#groupScrollView').kendoScrollView({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                total: 'total',
                data: 'data'
            },
            pageSize: 3
        },
        template:
            '<div class="row px-3">' +
                '# for (var i = 0; i < data.length; i++) { #' +
                    '<div class="col-4 text-center">' +
                        '<img class="img-thumbnail mt-3 mb-2" src="#= data[i].photo.url #" alt="#= data[i].nickName #">' +
                        '<picture style="background-image: url(\'#= data[i].photo.url #\');"></picture>' +
                        '<h5 class="font-weight-bold">#= data[i].realName #</h5>' +
                    '</div>' +
                '# } #' +
            '</div>',
        contentHeight: 240
    });
});